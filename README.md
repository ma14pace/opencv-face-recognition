# Python3 OpenCV Face Recognition
## Getting Started
These instructions will get you a copy of the project up and running on your local machine.
It is a modified version from https://github.com/Mjrovai/OpenCV-Face-Recognition
- dataset:
Contains sample images from our team.
- trainer/trainer.yml:
Training file from the sample images.
- FaceDetection:
This directory contains sample scripts to detect faces, eyes and smiles.
### Prerequisites
python3, opencv
#### Ubuntu:
```
$ apt-get install python3
$ pip3 install opencv
```
Each script detect different parts of the face as the script is named.
- FacialRecognition:
This directory contains 3 scripts to train/ recognize faces.
### Prerequisites
python3, opencv, opencv-contrib-python, libhdf5-dev
#### Ubuntu:
```
$ apt-get install python3 libhdf5-dev
$ pip3 install opencv opencv-contrib-python
```
### Get face image:
```
$ python3 01_face_dataset.py
```
Executing this script you need to enter an ID and look into the camera (cv2.VideoCapture(0) = intern camera; cv2.VideoCapture(1) = extern camera) for some seconds. After the script finishes it stops itself. Images are safed in the dataset directory.
Attention: You need at least 2 sample faces for the next step
### Training:
```
$ python3 02_face_training.py
```
After few seconds this script tried to write the trainer/trainer.yml file, which contains the training samples from our first step.
### Recognition:
```
$ python3 03_face_recognition.py
```
This script starts the camera again and tries to recognize the trained faces.
